# M169-Services Dokumentation David Brixel

## Einleitung Allgemein

In der folgenden Dokumentation werde ich mein Arbeitsverlauf des Moduls 169 erläutern. Dabei werden Bilder und andere Darstellungen verwendet

## Inhaltsverzeichnis

<!-- TOC -->

- [M169-Services Dokumentation David Brixel](#m169-services-dokumentation-david-brixel)
  - [Einleitung Allgemein](#einleitung-allgemein)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
    - [Toolumgebung-10](#toolumgebung-10)
    - [Infrastruktur Automatisierung-20](#infrastruktur-automatisierung-20)
    - [Infrastruktur Sicherheit-25](#infrastruktur-sicherheit-25)
    - [Container-30](#container-30)
    - [Add-ons-40](#add-ons-40)
    - [Reflexion-50](#reflexion-50)
    - [Quellen](#quellen)

<!-- /TOC -->

### Toolumgebung-10

Mit dem vorbereiten der Toolumgebung begann das Modul so richtig.

Hier die benötigte Software:

- Visual Studio Code
- Gitbash
- Gitlab
- Virtual Box
- Vagrant
- Docker

1. Der erste Schritt war ein eigenes Gitlab repository zu erstellen und dieses während des gesamten Modules führen. Dafür kann man ganz einfach auf die Gitlab Website und dort ein neues Projekt erstellen.

2. Um lokal mit Markdown arbeiten zu können muss das Projekt lokal zu verfügung gestellt werden. Im Projekt kann einfach auf "clonen" geklickt werden und mit https in einen lokalen folder gespeichert werden. Ganz einfach kann man auf Visual Studio Code (HTTPS klicken und so mit ein paar wenigen Klicks das Projekt lokal klonen)

![](./images/3.png)

3. Nun kann man im README File seine Dokumentation schreiben. Wenn man diese wieder auf Gitlab pushen will braucht man dafür Git-Bash und 3 Kommandos. Dafür muss man im Verzeichnis im lokalen Projekt sein.
   ![](./images/4.png)

```
git add .

git commit -m "commit"

git push -u origin main
```

### Infrastruktur Automatisierung-20

### Infrastruktur Sicherheit-25

### Container-30

![](./images/2.png)

Das Ziel ist es, dass die Umgebung mit Container am Schluss so aussehen sollte. Nur die Ports sind abgeändert und variabel.

Mit der VPN Connection von Wireguard können wir auf die TBZ VM über ssh in die VM gelangen. Das Container Image ist in unserem Fall im Registry auf dem eigenen Gitlab Repository gespeichert worden. Dadurch kann man den Container und das lokale Image jederzeit löschen und vom Image auf Gitlab einen neuen Container laufen lassen, ganz im Gegensatz zur VM, bei welcher zwar Snapshots machen kann, welche aber immer wieder eingeplant werden müssen und Zeit und Speicherplatz beanspruchen. Das Ziel ist, dass man über den Browser auf den Host über den Port 8080 oder bei uns 8090 auf den Port 8095 weitergeleitet wird und dort eine Web-App erstellt worden ist.

wichtige Befehle:

```
#docker image builden/erstellen
docker image build -t registry.gitlab.com/[Benutzer]/[Repository]/[container name]:[tag] .

#docker image auf gitlab hochladen
docker image push registry.gitlab.com/[Benutzer]/[Repository]/[container name]:[tag]

#falls die autentication nicht geht folgenden befehl benutzen
docker login registry.gitlab.com -u [username]

#Container mit dem Image von Gitlab erstellen
docker container run --name [Container Name] -d -p [Hostport]:[VM-Port] registry.gitlab.com/[Benutzer]/[Repository]/[container name]:[tag]

#alle images anzeigen lassen
Docker image ls

#spezifisches image löschen
Docker image rm [ID]

#laufenden Container stoppen
docker container stop [ID]

#gestoppten Container löschen
docker container rm [ID]

#Container stoppen und löschen
docker container rm [ID]

#vorhandenen Container wieder laufen lassen
Docker container start [ID]

#laufende Container anzeigen lassen
docker ps

#alle container anzeigen lassen
docker ps -a
```

Schritte Abfolge webapp_one:

1. Gitlab-Repo runterladen (beinhaltet Dockerfile, app.js,...)

```
git clone https://gitlab.com/ser-cal/Container-CAL-webapp_v1.git
```

2. Cd in das lokale Repository Container-CAL-webapp_v1 und danach noch cd in App

```
cd Container-CAL-webapp_v1 und cd App
```

3. Image lokal erstellen

```
docker image build -t registry.gitlab.com/david902/m169/webapp_one:1.0 .
```

4. Image auf Gitlab registry pushen

```
docker image push registry.gitlab.com/david902/m169/webapp_one:1.0
```

5. Auf Gitlab im Repository nachschauen, ob image gepusht wurde (packages and registries -> Container Registry)
   <br>
   <br>
6. Container erstellen und gleich laufen lassen

```
docker container run -d --name dav-web -p 8090:8095 registry.gitlab.com/david902/m169/webapp_one:1.0
```

### Add-ons-40

### Reflexion-50

### Quellen

In diesem Modul war ich natürlich auf Hilfe angewiesen. Dabei war mir das Gitlab Repository von Herrn Callisto eine grosse Hilfe. Vor allem die jeweiligen Erklärvideos mit den dazugehörigen Befehlen im txt-File konnten mir sehr weiterhelfen. Teilweise habe ich kleiner Hilfe von Mitschülern angenommen, viele Fehler konnte ich jedoch mit knobeln und ausprobieren lösen. Ausserdem konnte ich das gelernte Wissen weitergeben an Mitschüler in der Klasse und ihnen als Hilfe beistehen.
