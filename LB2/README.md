# LB2 Dokumentation

## Inhaltsverzeichnis

<!-- TOC -->

- [LB2 Dokumentation](#lb2-dokumentation)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Einleitung](#einleitung)
  - [Service-Beschreibung](#service-beschreibung)
  - [Umsetzung](#umsetzung)
    - [Web-Applikation](#web-applikation)
    - [Dockerfile und Image](#dockerfile-und-image)
    - [Docker compose](#docker-compose)
  - [Quellen](#quellen)

<!-- /TOC -->

## Einleitung

Im Modul 169 wird mit Containern gearbeitet, um diese besser kennenzulernen werden Virtual Machines gebraucht auf welchen mehrere Container zur gleichen Zeit laufen. Dadurch kann jeder Schüler ein individuelles Projekt erarbeiten, welches mit dem Thema Container in Verbindung gebracht wird. Es könnten Projekte entstehen, wie eine Registrierungs Web-App in meinem Fall.

Falls Fragen zu den Grundlagen der Container, oder zum erstellen eines Gitlab Repositorys entstehen, kann jeder Zeit auf das Readme File des gesamten Moduls zu gegriffen werden, in welchen der Ablauf einer einfachen Container Web-Applikation genau geschildert ist.

## Service-Beschreibung

In der LB2 werde ich mithilfe von Docker und Containern eine Registrierungs Web-App erstellen. Wenn sich der User auf der Web-Applikation (in einem Container) registriert und auf den Submit Button geklickt wird, erscheint eine neue Seite, bei welcher die erfolgreiche Registrierung bestätigt wird. Dazu werden die Regestrierungsdaten in eine SQL Datenbank gespeichert. In der Umsetzung wird dabei eine .yaml Konfigurationsdatei, ein Dockerfile, eine .php Datei für die Web-App und eine HTML Datei für die Bestätigungsseite verwendet.
![](./images/5.png)

## Umsetzung

In diesem Kapitel werde ich den ganzen Arbeitsprozess festhalten und in einzelne Teile gliedern, welche ich chronologisch erarbeitet habe.

### Web-Applikation

Der erste Schritt des Projektes, war es für mich eine funktionierende PHP Registrierungs Web-Applikation zu erstellen. Dazu habe ich lokal die Web-App geskriptet und nachher mit Copy-Paste den inhalt in die VM kopiert.

Mithilfe der "Form" kann man eine einfache Registrierungs Web-App erstellen. Ich werde nicht genauer auf den HTML code eingehen, da es im Internet viele Quellen gibt.

```
<!DOCTYPE html>
<html>
  <head> </head>
  <body>
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <form method= "post" action="">
      <label for="Email">E-Mail</label><br />
      <input
        type="email"
        id="email"
        name="email"
        placeholder="enter Email"
      /><br />
      <label for="password">password</label><br />
      <input
        type="text"
        id="password"
        name="password"
        placeholder="enter password"
      /><br /><br />
      <input type="submit" value="Submit" />
    </form>
  </body>
</html>
```

![](./images/1.png)

Wenn in der Applikation auf den Submit Button geklickt wird, wird nachher dieses HTML File angezeigt, auf welcher die Registrierung bestätigt wird. Im PHP code findet die weiterleitung zu dieser Datei statt.

```
<!DOCTYPE html public>
<html>
  <head>
    Success Page
  </head>
  <body>
    <h1>You have been registered</h1>
    <p>You can close this tab now</p>
  </body>
</html>
```

![](./images/3.png)

Der PHP Teil der Webpage ist auch im index.php file zusammen mit dem HTML code gespeichert. Zuerst wird eine Connection mit der DB erstellt. Wenn diese Connection nicht hergestellt werden kann gibt die Web Applikation nur noch "Connection failed" an und den bestehenden Error. Falls die Registrierungs App nicht komplett mit Passwort und Email ausgefüllt wird, wird "Please complete the registration form!" angezeigt. Falls nun alle benötigten Daten eingegeben wurden. Wird in die Datenbank der Email und Passwot Datensatz eingefügt. Wenn das geklappt hat wird nachher die andere Seite success.html angezeigt. Wenn es aber nicht geklappt hat wird der Error angezeigt.

```
<?php
$servername = "db";
$username = "root";
$password = "password";

$db = "lb2";

// Create connection
$conn = new mysqli($servername, $username, $password, $db);
// Check connection
if ($conn->connect_error) {
  echo "connection failed";
  die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (!isset($_POST['email'], $_POST['password'])) {
// Could not get the data that should have been sent.
        echo('Please complete the registration form!');
  }
  else {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $sql = "INSERT INTO register (email, password)
        VALUES ('$email', '$password')";

        if ($conn->query($sql) === TRUE) {
        header('Location: ./success.html');
        }
        else {
        echo "Error: " . $sql . "<br>" . $conn->error;
       }
  }
}
?>
```

### Dockerfile und Image

Im Dockerfile sind wenige Informationen, wie das php Image, welches verwendet wird. Es wird mysqli installiert und benutzt.

```
FROM php:8.0-apache
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN apt-get update && apt-get upgrade -y
```

Ich habe danach das Dockefile in ein Image verpackt und auf Gitlab verpackt. Name: webapp-lb2

```
docker image build -t registry.gitlab.com/david902/m169/webapp-lb2:1.0 .
docker image push registry.gitlab.com/david902/m169/webapp-lb2:1.0
```

### Docker compose

Im .yaml File werden die App und die Datenbank definiert. mit den untenstehenden Befehlen kann der ganze Service schnell gestoppt werden und direkt wieder aufgestartet werden.

Die Applikation wird vom Registry Image gebaut, der Name ist einfach app, das Persistant Volume src ist für den Container lokal verfügbar unter /var/www/html. Und als Port wird 8090 benutzt.

Die Datenbank wird vom mysql Image gebaut. Der Server heisst db und ein Benutzer "user" wird erstellt. Das Passwort für den user und den root ist password. Es wird der Port 3306 verwendet und es wird wie schon bei der Web-App ein Persistant volumes my-db eingebaut.

```
version: '3'
services:

  app:
    build: https://gitlab.com/david902/m169/container_registry/4172623
    container_name: app
    restart: always
    volumes:
      - ./src:/var/www/html
    ports:
      - 8090:80

  db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_DATABASE: 'db'
      # So you don't have to use root, but you can if you like
      MYSQL_USER: 'user'
      # You can use whatever password you like
      MYSQL_PASSWORD: 'password'
      # Password for root access
      MYSQL_ROOT_PASSWORD: 'password'
    ports:
      # <Port exposed> : <MySQL Port running inside container>
      - '3306:3306'
    volumes:
      - ./my-db:/var/lib/mysql

```

```
docker-compose up -d
docker-compose down
```

## Quellen

In diesem Kapitel werden alle Quellen aufgelistet, welche in diesem Projekt verwendet wurden

- [Netork Chuck Docker Compose](https://www.youtube.com/watch?v=DM65_JyGxCo&list=PLAMz53v4GaWltbZndYvWPeW87L4HSyYX1&index=13)
- [Docker Image for HTML](https://www.youtube.com/watch?v=UXAoZg1W3Q4&list=PLAMz53v4GaWltbZndYvWPeW87L4HSyYX1&index=12)
- [PHP Web Applikation](https://www.youtube.com/watch?v=JPFrxV1TVfM)
- Joseph hat mir in der LB2 ein paar hilfreiche Tipps gegeben, welche mir sehr weitergeholfen haben
